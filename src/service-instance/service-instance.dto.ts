export class ServiceInstanceDTO {
  name: string;
  url: string;
}
