import { Test, TestingModule } from '@nestjs/testing';
import { ServiceInstanceService } from './service-instance.service';

describe('ServiceInstanceService', () => {
  let service: ServiceInstanceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServiceInstanceService],
    }).compile();

    service = module.get<ServiceInstanceService>(ServiceInstanceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
