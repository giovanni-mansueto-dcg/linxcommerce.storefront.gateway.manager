import { Injectable } from '@nestjs/common';
import {
  Repository,
  InjectRepository,
  AzureTableStorageResultList,
  AzureTableStorageResponse,
} from '@nestjs/azure-database';
import { ServiceInstance } from './service-instance.entity';

@Injectable()
export class ServiceInstanceService {
  constructor(
    @InjectRepository(ServiceInstance)
    private readonly serviceInstanceRepository: Repository<ServiceInstance>,
  ) {}

  // find one serviceInstance entitu by its rowKey
  async find(
    rowKey: string,
    serviceInstance: ServiceInstance,
  ): Promise<ServiceInstance> {
    return this.serviceInstanceRepository.find(rowKey, serviceInstance);
  }

  // find all serviceInstance entities
  async findAll(): Promise<AzureTableStorageResultList<ServiceInstance>> {
    return this.serviceInstanceRepository.findAll();
  }

  // find by name serviceInstance entities
  async findByName(
    name: string,
  ): Promise<AzureTableStorageResultList<ServiceInstance>> {
    return this.serviceInstanceRepository
      .top(1)
      .where('name == ?', name)
      .findAll();
  }

  // find by url serviceInstance entities
  async findByUrl(
    url: string,
  ): Promise<AzureTableStorageResultList<ServiceInstance>> {
    return this.serviceInstanceRepository
      .top(1)
      .where('url == ?', url)
      .findAll();
  }

  // create a new serviceInstance entity
  async create(serviceInstance: ServiceInstance): Promise<ServiceInstance> {
    return this.serviceInstanceRepository.create(serviceInstance);
  }

  // update the a serviceInstance entity by its rowKey
  async update(
    rowKey: string,
    serviceInstance: Partial<ServiceInstance>,
  ): Promise<ServiceInstance> {
    return this.serviceInstanceRepository.update(rowKey, serviceInstance);
  }

  // delete a serviceInstance entity by its rowKey
  async delete(
    rowKey: string,
    serviceInstance: ServiceInstance,
  ): Promise<AzureTableStorageResponse> {
    return this.serviceInstanceRepository.delete(rowKey, serviceInstance);
  }
}
