import {
  EntityPartitionKey,
  EntityRowKey,
  EntityString,
} from '@nestjs/azure-database';

@EntityPartitionKey('ServiceInstanceID')
@EntityRowKey('ServiceInstanceName')
export class ServiceInstance {
  @EntityString() name: string;
  @EntityString() url: string;
  //@EntityIn32() age: number;
}
