import { Test, TestingModule } from '@nestjs/testing';
import { ServiceInstanceController } from './service-instance.controller';

describe('ServiceInstanceController', () => {
  let controller: ServiceInstanceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServiceInstanceController],
    }).compile();

    controller = module.get<ServiceInstanceController>(
      ServiceInstanceController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
