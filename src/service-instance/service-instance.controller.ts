import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UnprocessableEntityException,
  NotFoundException,
  Patch,
} from '@nestjs/common';
import { ServiceInstanceDTO } from './service-instance.dto';
import { ServiceInstance } from './service-instance.entity';
import { ServiceInstanceService } from './service-instance.service';

@Controller('serviceInstances')
export class ServiceInstanceController {
  constructor(
    private readonly serviceInstanceService: ServiceInstanceService,
  ) {}

  @Get()
  async getAllServiceInstances() {
    return await this.serviceInstanceService.findAll();
  }

  @Get('name/:name')
  async getServiceInstancesByName(@Param('name') name) {
    if (!name) {
      throw new NotFoundException('Name not found');
    }

    return await this.serviceInstanceService.findByName(name);
  }

  @Get(':rowKey')
  async getServiceInstance(@Param('rowKey') rowKey) {
    try {
      return await this.serviceInstanceService.find(
        rowKey,
        new ServiceInstance(),
      );
    } catch (error) {
      // Entity not found
      throw new NotFoundException(error);
    }
  }

  @Post()
  async createServiceInstance(
    @Body()
    serviceInstanceData: ServiceInstanceDTO,
  ) {
    try {
      const serviceInstance = new ServiceInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(serviceInstance, serviceInstanceData);

      return await this.serviceInstanceService.create(serviceInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Put(':rowKey')
  async saveServiceInstance(
    @Param('rowKey') rowKey,
    @Body() serviceInstanceData: ServiceInstanceDTO,
  ) {
    try {
      const serviceInstance = new ServiceInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(serviceInstance, serviceInstanceData);

      return await this.serviceInstanceService.update(rowKey, serviceInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Patch(':rowKey')
  async updateServiceInstanceDetails(
    @Param('rowKey') rowKey,
    @Body() serviceInstanceData: Partial<ServiceInstanceDTO>,
  ) {
    try {
      const serviceInstance = new ServiceInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(serviceInstance, serviceInstanceData);

      return await this.serviceInstanceService.update(rowKey, serviceInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Delete(':rowKey')
  async deleteDelete(@Param('rowKey') rowKey) {
    try {
      const response = await this.serviceInstanceService.delete(
        rowKey,
        new ServiceInstance(),
      );

      if (response.statusCode === 204) {
        return null;
      } else {
        throw new UnprocessableEntityException(response);
      }
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }
}
