import { Module } from '@nestjs/common';
import { AzureTableStorageModule } from '@nestjs/azure-database';
import { ServiceInstanceService } from './service-instance.service';
import { ServiceInstanceController } from './service-instance.controller';
import { ServiceInstance } from './service-instance.entity';

@Module({
  imports: [
    AzureTableStorageModule.forFeature(ServiceInstance, {
      table:
        (process.env.AZURE_STORAGE_TABLE_NAME || 'apollogatewaymanager') +
        'services',
      createTableIfNotExists: true,
    }),
  ],
  providers: [ServiceInstanceService],
  controllers: [ServiceInstanceController],
})
export class ServiceInstanceModule {}
