import { Module } from '@nestjs/common';
import { AuthModule } from 'linxcommerce-nestjs-shared/auth';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServiceInstanceModule } from './service-instance/service-instance.module';
import { GatewayInstanceModule } from './gateway-instance/gateway-instance.module';

@Module({
  imports: [AuthModule, ServiceInstanceModule, GatewayInstanceModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
