import { Controller, Get, UseGuards, Request } from '@nestjs/common';
import {
  RolesGuard,
  Roles,
  JwtIdADAuthGuard,
  JwtOAuthAuthGuard,
} from 'linxcommerce-nestjs-shared/auth';

import { OnlyReader } from './roles';

@Controller()
export class AppController {
  @Get()
  getHello(): any {
    return {
      hello: true,
    };
  }

  @UseGuards(JwtIdADAuthGuard, RolesGuard)
  @Get('profile')
  @Roles('admin')
  getProfile(@Request() req) {
    return req.user;
  }

  @UseGuards(JwtOAuthAuthGuard, RolesGuard)
  @Get('profile-oauth')
  @OnlyReader()
  getProfileOauth(@Request() req) {
    return req.user;
  }
}
