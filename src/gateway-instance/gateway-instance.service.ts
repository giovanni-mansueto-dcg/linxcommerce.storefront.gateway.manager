import { Injectable } from '@nestjs/common';
import {
  Repository,
  InjectRepository,
  AzureTableStorageResultList,
  AzureTableStorageResponse,
} from '@nestjs/azure-database';
import { GatewayInstance } from './gateway-instance.entity';

@Injectable()
export class GatewayInstanceService {
  constructor(
    @InjectRepository(GatewayInstance)
    private readonly gatewayInstanceRepository: Repository<GatewayInstance>,
  ) {}

  // find one gatewayInstance entitu by its rowKey
  async find(
    rowKey: string,
    gatewayInstance: GatewayInstance,
  ): Promise<GatewayInstance> {
    return this.gatewayInstanceRepository.find(rowKey, gatewayInstance);
  }

  // find all gatewayInstance entities
  async findAll(): Promise<AzureTableStorageResultList<GatewayInstance>> {
    return this.gatewayInstanceRepository.findAll();
  }

  // find by name gatewayInstance entities
  async findByName(
    name: string,
  ): Promise<AzureTableStorageResultList<GatewayInstance>> {
    return this.gatewayInstanceRepository
      .top(1)
      .where('name == ?', name)
      .findAll();
  }

  // find by url gatewayInstance entities
  async findByUrl(
    url: string,
  ): Promise<AzureTableStorageResultList<GatewayInstance>> {
    return this.gatewayInstanceRepository
      .top(1)
      .where('url == ?', url)
      .findAll();
  }

  // create a new gatewayInstance entity
  async create(gatewayInstance: GatewayInstance): Promise<GatewayInstance> {
    return this.gatewayInstanceRepository.create(gatewayInstance);
  }

  // update the a gatewayInstance entity by its rowKey
  async update(
    rowKey: string,
    gatewayInstance: Partial<GatewayInstance>,
  ): Promise<GatewayInstance> {
    return this.gatewayInstanceRepository.update(rowKey, gatewayInstance);
  }

  // delete a gatewayInstance entity by its rowKey
  async delete(
    rowKey: string,
    gatewayInstance: GatewayInstance,
  ): Promise<AzureTableStorageResponse> {
    return this.gatewayInstanceRepository.delete(rowKey, gatewayInstance);
  }
}
