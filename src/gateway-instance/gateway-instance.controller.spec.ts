import { Test, TestingModule } from '@nestjs/testing';
import { GatewayInstanceController } from './gateway-instance.controller';

describe('GatewayInstanceController', () => {
  let controller: GatewayInstanceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GatewayInstanceController],
    }).compile();

    controller = module.get<GatewayInstanceController>(
      GatewayInstanceController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
