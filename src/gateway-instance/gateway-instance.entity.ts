import {
  EntityPartitionKey,
  EntityRowKey,
  EntityString,
} from '@nestjs/azure-database';

@EntityPartitionKey('GatewayInstanceID')
@EntityRowKey('GatewayInstanceName')
export class GatewayInstance {
  @EntityString() name: string;
  @EntityString() url: string;
}
