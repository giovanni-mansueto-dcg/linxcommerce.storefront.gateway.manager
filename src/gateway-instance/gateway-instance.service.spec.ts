import { Test, TestingModule } from '@nestjs/testing';
import { GatewayInstanceService } from './gateway-instance.service';

describe('GatewayInstanceService', () => {
  let service: GatewayInstanceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GatewayInstanceService],
    }).compile();

    service = module.get<GatewayInstanceService>(GatewayInstanceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
