import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UnprocessableEntityException,
  NotFoundException,
  Patch,
} from '@nestjs/common';
import { GatewayInstanceDTO } from './gateway-instance.dto';
import { GatewayInstance } from './gateway-instance.entity';
import { GatewayInstanceService } from './gateway-instance.service';

@Controller('gatewayInstances')
export class GatewayInstanceController {
  constructor(
    private readonly gatewayInstanceService: GatewayInstanceService,
  ) {}

  @Get()
  async getAllGatewayInstances() {
    return await this.gatewayInstanceService.findAll();
  }

  @Get('name/:name')
  async getGatewayInstancesByName(@Param('name') name) {
    if (!name) {
      throw new NotFoundException('Name not found');
    }

    return await this.gatewayInstanceService.findByName(name);
  }

  @Get(':rowKey')
  async getGatewayInstance(@Param('rowKey') rowKey) {
    try {
      return await this.gatewayInstanceService.find(
        rowKey,
        new GatewayInstance(),
      );
    } catch (error) {
      // Entity not found
      throw new NotFoundException(error);
    }
  }

  @Post()
  async createGatewayInstance(
    @Body()
    gatewayInstanceData: GatewayInstanceDTO,
  ) {
    try {
      const gatewayInstance = new GatewayInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(gatewayInstance, gatewayInstanceData);

      return await this.gatewayInstanceService.create(gatewayInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Put(':rowKey')
  async saveGatewayInstance(
    @Param('rowKey') rowKey,
    @Body() gatewayInstanceData: GatewayInstanceDTO,
  ) {
    try {
      const gatewayInstance = new GatewayInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(gatewayInstance, gatewayInstanceData);

      return await this.gatewayInstanceService.update(rowKey, gatewayInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Patch(':rowKey')
  async updateGatewayInstanceDetails(
    @Param('rowKey') rowKey,
    @Body() gatewayInstanceData: Partial<GatewayInstanceDTO>,
  ) {
    try {
      const gatewayInstance = new GatewayInstance();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(gatewayInstance, gatewayInstanceData);

      return await this.gatewayInstanceService.update(rowKey, gatewayInstance);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Delete(':rowKey')
  async deleteDelete(@Param('rowKey') rowKey) {
    try {
      const response = await this.gatewayInstanceService.delete(
        rowKey,
        new GatewayInstance(),
      );

      if (response.statusCode === 204) {
        return null;
      } else {
        throw new UnprocessableEntityException(response);
      }
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }
}
