import { Module } from '@nestjs/common';
import { AzureTableStorageModule } from '@nestjs/azure-database';
import { GatewayInstanceService } from './gateway-instance.service';
import { GatewayInstanceController } from './gateway-instance.controller';
import { GatewayInstance } from './gateway-instance.entity';

@Module({
  imports: [
    AzureTableStorageModule.forFeature(GatewayInstance, {
      table:
        (process.env.AZURE_STORAGE_TABLE_NAME || 'apollogatewaymanager') +
        'gateways',
      createTableIfNotExists: true,
    }),
  ],
  providers: [GatewayInstanceService],
  controllers: [GatewayInstanceController],
})
export class GatewayInstanceModule {}
