import { Roles } from 'linxcommerce-nestjs-shared/auth';

export const OnlyReader = () => Roles('Auto.Service.A.Reader');
